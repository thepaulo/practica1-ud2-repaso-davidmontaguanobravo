package com.david.tiendacocinas.base.enums.proveedor;


/*
Clase enum de los años en el mercado de los proveedore para posteriormente seleccinarlos
 */
public enum AniosMercado {


    uno("1 año"),
    cinco("5 años"),
    diez("10 años"),
    veinte("20 años"),
    masveinte("+20 años");

    private String valor;

    AniosMercado(String valor){
        this.valor= valor;

    }
    public String getValor(){
        return valor;
    }
}
