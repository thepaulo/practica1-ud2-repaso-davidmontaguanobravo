package com.david.tiendacocinas.base.enums.proveedor;


/*
Clase enum de la nacionalidad  de los proveedore para posteriormente seleccinarlos
 */
public enum Nacionalidad {

    ESPANIA("ESPAÑA"),
    ALEMANIA("ALEMANIA"),
    CHINA("CHINA"),
    USA("USA"),
    JAPON("JAPON"),
    ITALIA("ITALIA"),
    COREADELNORTE("COREADELNORTE");

    private String valor;

    Nacionalidad(String valor){
        this.valor= valor;

    }
    public String getValor(){
        return valor;
    }
}
