package com.david.tiendacocinas.base.enums.proveedor;


/*
Clase enum de los nombres de los proveedore para posteriormente seleccinarlos
 */
public enum NProveedor {
    AMAZON("AMAZON"),
    EBAY("EBAY"),
    MERCADOLIBRE("CANDY"),
    BOSCH("BOSCH"),
    ELCORTEINGLES("EL CORTE INGLES"),
    MEDIAMARK("MEDIAMARK"),
    FAGOR("FAGOR"),
    BALAY("BALAY");

    private String valor;

    NProveedor(String valor){
        this.valor= valor;

    }
    public String getValor(){
        return valor;
    }
}
