package com.david.tiendacocinas.base.enums.electrodomestico;

/*
Clase enum de los diferentes electrodomesticos de los productos para posteriormente seleccinarlos
 */
public enum NElectrodomestico {

    LAVADORA("LAVADORA"),
    HORNO("HORNO"),
    NEVERA("NEVERA"),
    LAVAVAJILLAS("LAVADORA"),
    TOSTADORA("TOSTADORA"),
    MICROONDAS("MICROONDAS"),
    SECADORA("SECADORA"),
    EXTRACTOR("EXTRACTOR"),
    FREIDORA("FREIDORA"),
    VITROCERAMICA("VITROCERAMICA");

    private String valor;

    NElectrodomestico(String valor){
        this.valor= valor;

    }
    public String getValor(){
        return valor;
    }
}
