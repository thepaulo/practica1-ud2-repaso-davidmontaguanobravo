package com.david.tiendacocinas.base.enums.electrodomestico;
/*
Clase enum de las marcas de los productos para posteriormente seleccinarlos
 */
public enum Marca {

    BALAY("BALAY"),
    BOSCH("BOSCH"),
    CANDY("CANDY"),
    FAGOR("FAGOR"),
    WHIRLPOOL("WHIRLPOOL"),
    LG("LG"),
    SAMSUNG("SAMSUNG");

    private String valor;

    Marca(String valor){
        this.valor= valor;

    }
    public String getValor(){
        return valor;
    }
}
