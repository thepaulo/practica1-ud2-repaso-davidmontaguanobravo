package com.david.tiendacocinas.base.enums.electrodomestico;
/*
Clase enum de las dimensiones de los productos para posteriormente seleccinarlos
 */
public enum Dimension {

    PEQUENIO("PEQUEÑO"),
    MEDIANO("MEDIANO"),
    GRANDE("GRANDE"),
    MASGRANDE("GIGANTE");

    private String valor;

    Dimension(String valor){
        this.valor= valor;

    }
    public String getValor(){
        return valor;
    }




}
