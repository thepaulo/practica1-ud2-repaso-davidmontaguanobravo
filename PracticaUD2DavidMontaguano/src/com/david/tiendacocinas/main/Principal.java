package com.david.tiendacocinas.main;

import com.david.tiendacocinas.gui.Controlador;
import com.david.tiendacocinas.gui.Modelo;
import com.david.tiendacocinas.gui.Vista;
/*
Clase cPrincipal donde llamaremos a las clases vista, modelo y controlador para ejecutar correctamente nuestra aplicacion
 */
public class Principal {
    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Vista vista = new Vista();
        Controlador controlador= new Controlador(modelo,vista);


    }
}
