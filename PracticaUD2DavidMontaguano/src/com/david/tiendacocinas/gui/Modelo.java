package com.david.tiendacocinas.gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {

    private String ip;
    private String root;
    private String password;
    private String adminPassword;

    public Modelo() {

        getPropValues();
    }

    String getIP() {

        return ip;
    }
    String getUser() {

        return root;
    }
    String getPassword() {

        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    private Connection conexion;
/*
EMtodo conectar
 */
    public void conectar() {
        try {
            conexion= DriverManager.getConnection("jdbc:mysql://"+ip+":3306/cocinasdavid","root","");

        }catch (SQLException e1){

            try {
                conexion=DriverManager.getConnection("jdbc:mysql://"+ip+":3306","root","");
                PreparedStatement statement=null;
                String code=leerFichero();
                String [] query=code.split("--");
                for (String aQuery: query){
                    statement =conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement !=null;
                statement.close();
                System.out.println("conexion realizada");
            }catch (SQLException | IOException e){
                e.printStackTrace();
            }
        }

    }
/*
MEtodo para leer el fichero
 */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("cocinasdavid.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();

        }
    }
/*
MEtodo desconectar
 */
    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }
/*
Metods que usaremos para dar de alta a un proveedor
 */
    public void insertarProveedor(String nombre_proveedor,String nacionalidad, String aniosMercado ) throws SQLException{
        String consulta ="INSERT INTO proveedor (nombre_proveedor,nacionalidad,anios_mercado) VALUES(?,?,?)";
        PreparedStatement sentencia=null;
        sentencia=conexion.prepareStatement(consulta);
        sentencia.setString(1,nombre_proveedor);
        sentencia.setString(2,nacionalidad);
        sentencia.setString(3,aniosMercado);
        sentencia.executeUpdate();
    }

    void modificarProveedor(String nacionalidad,String nombre_proveedor, String aniosMercado , int id_proveedor)throws SQLException{
        String sentenciasql ="UPDATE proveedor SET nacionalidad=?,nombre_proveedor=?,anios_mercado=? WHERE id_proveedor=?";
        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(sentenciasql);
        sentencia.setString(1,nacionalidad);
        sentencia.setString(2,nombre_proveedor);
        sentencia.setString(3,aniosMercado);
        sentencia.setInt(4,id_proveedor);
        sentencia.executeUpdate();

    }

    ResultSet consultarProveedor()throws SQLException{
        String sentenciaSql="SELECT concat(id_proveedor) as 'Id',concat(nacionalidad) as 'Nacionalidad', concat(nombre_proveedor) as 'Nombre Proveedor', concat(anios_mercado) as' Anios Mercado' FROM proveedor";
        PreparedStatement sentencia=null;
        ResultSet resultado=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        resultado=sentencia.executeQuery();
        return resultado;
    }

    void borrarProveedor(int id_proveedor){
        String sentenciaSql="DELETE FROM proveedor WHERE id_proveedor=?";
        PreparedStatement sentencia=null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,id_proveedor);
            sentencia.executeUpdate();

        }catch (SQLException e1){
            e1.printStackTrace();
        }finally {
            if(sentencia!=null){
                try {
                    sentencia.close();

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
            }
        }
    }

    /*
    Metods que usaremos para dar de alta a una VEnta
     */
    void insertarVenta(String nombre_cliente, String direccion, String electrodomestico , String proveedor, float precio, LocalDate fecha_factura)throws SQLException{
        String sentenciasql="INSERT INTO venta(nombre_cliente,direccion,id_electrodomestico,id_proveedor,precio,fecha_factura)"+
                "VALUES(?,?,?,?,?,?)";
        PreparedStatement sentencia=null;

        int id_electrodomestico=Integer.valueOf(electrodomestico.split(" ")[0]);
        int id_proveedor=Integer.valueOf(proveedor.split("")[0]);


        sentencia=conexion.prepareStatement(sentenciasql);
        sentencia.setString(1,nombre_cliente);
        sentencia.setString(2,direccion);
        sentencia.setInt(3,id_electrodomestico);
        sentencia.setInt(4,id_proveedor);
        sentencia.setFloat(5,precio);
        sentencia.setDate(6, Date.valueOf(fecha_factura));
        sentencia.executeUpdate();

    }

    ResultSet consultarVenta()throws SQLException{
        String sentenciaSql="SELECT concat(id_venta) as 'Id',concat(nombre_cliente)as 'Nombre Cliente',concat(direccion)as'Direccion'," +
                "concat(id_electrodomestico)as'Electrodomestico',concat(id_proveedor)as'Proveedor',concat(precio)as'Precio'" +
                ",concat(fecha_factura)as'Fecha Factura' FROM venta";
        PreparedStatement sentencia=null;
        ResultSet resultado=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        resultado=sentencia.executeQuery();
        return resultado;
    }

    void modificarVenta(String nombre_cliente, String direccion, String electrodomestico , String proveedor, float precio, LocalDate fecha_factura,
                        int id_venta)throws SQLException{
        String sentenciasql="UPDATE venta SET nombre_cliente=?,direccion?,id_electrodomestico=?,id_proveedor=?,precio=?,fecha_factura=?"+
                "WHERE id_venta=?";
        PreparedStatement sentencia=null;

        int id_electrodomestico=Integer.valueOf(electrodomestico.split(" ")[0]);
        int id_proveedor=Integer.valueOf(proveedor.split("")[0]);

        sentencia=conexion.prepareStatement(sentenciasql);
        sentencia.setString(1,nombre_cliente);
        sentencia.setString(2,direccion);
        sentencia.setInt(3,id_electrodomestico);
        sentencia.setInt(4,id_proveedor);
        sentencia.setFloat(5,precio);
        sentencia.setDate(6, Date.valueOf(fecha_factura));
        sentencia.setInt(7,id_venta);
        sentencia.executeUpdate();



    }

    void borrarVenta (int id_venta){
        String sentenciaSql="DELETE FROM venta WHERE id_venta=?";
        PreparedStatement sentencia=null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,id_venta);
            sentencia.executeUpdate();

        }catch (SQLException e1){
            e1.printStackTrace();
        }finally {
            if(sentencia!=null){
                try {
                    sentencia.close();

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
            }
        }

    }

    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            root = prop.getProperty("root");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    void setPropValues(String ip, String root, String pass, String passAd) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", root);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", passAd);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.root = root;
        this.password = pass;
        this.adminPassword = passAd;
    }
    /*
    Metods que usaremos para dar de alta a un Electrodoemstico
     */
    void insertarElectrodomestico(String nombre_electrodomestico,String cantidad,String marca,String dimension) throws SQLException{
        String sentenciasql="INSERT INTO electrodomestico(nombre_electrodomestico,cantidad,marca,dimension) VALUES(?,?,?,?)";
        PreparedStatement sentencia=null;
        sentencia=conexion.prepareStatement(sentenciasql);
        sentencia.setString(1,nombre_electrodomestico);
        sentencia.setString(2,cantidad);
        sentencia.setString(3,marca);
        sentencia.setString(4,dimension);
        sentencia.executeUpdate();

    }
    ResultSet consultarElectrodomestico()throws SQLException{
        String sentenciaSql="SELECT concat(id_electrodomestico) as 'Id',concat(nombre_electrodomestico)as'Nombre Electromestico',concat(cantidad) as 'Cantidad'" +
                ", concat(marca) as 'Marca',concat(dimension)as'Dimension'FROM electrodomestico";
        PreparedStatement sentencia=null;
        ResultSet resultado=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        resultado=sentencia.executeQuery();
        return resultado;
    }

    void modificarElectrodomestico(String nombre_electrodomestico,String cantidad,String marca,String dimension, int id_electrodomestico)throws SQLException{
        String sentenciasql="UPDATE electrodomestico SET nombre_electrodomestico=?,cantidad=?,marca=?,dimension=? WHERE id_electrodomestico=?";
        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(sentenciasql);
        sentencia.setString(1,nombre_electrodomestico);
        sentencia.setString(2,cantidad);
        sentencia.setString(3,marca);
        sentencia.setString(4,dimension);
        sentencia.setInt(5,id_electrodomestico);
        sentencia.executeUpdate();

    }


    void borrarElectrodomestico(int id_electrodomestico){
        String sentenciaSql="DELETE FROM electrodomestico WHERE id_electrodomestico=?";
        PreparedStatement sentencia=null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,id_electrodomestico);
            sentencia.executeUpdate();

        }catch (SQLException e1){
            e1.printStackTrace();
        }finally {
            if(sentencia!=null){
                try {
                    sentencia.close();

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
            }
        }
    }

    public ResultSet buscarVenta(String nombreCliente )throws SQLException{
        if (conexion==null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consulta = "SELECT * FROM cocinasdavid.venta where nombre_cliente =?";
        PreparedStatement sentencia=null;

        sentencia =conexion.prepareStatement(consulta);
        sentencia.setString(1,(nombreCliente));
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    public ResultSet clientePorNombre() throws SQLException {

        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta="SELECT * FROM venta ORDER BY nombre_cliente";
        PreparedStatement sentencia= null;
        sentencia= conexion.prepareStatement(consulta);


        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    public ResultSet obtenerDatos() throws SQLException {
        if (conexion==null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT nombre_cliente,COUNT(*) FROM cocinasdavid.venta GROUP BY nombre_cliente";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    public void crearNuevaTabla() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/cocinasdavid","root","");
        String sentenciaSql="call crearTablaCocinas2()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
       procedimiento.execute();
    }

}
