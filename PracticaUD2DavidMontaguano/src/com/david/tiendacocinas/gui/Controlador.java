package com.david.tiendacocinas.gui;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;
/*
Esta es la clase controlador en la cual podemos tenemos los metodos ActionListener, ItemListener, ListSelectionListener, WindowListener
para una correcta funcionalidad tanto de los apartados como de los botones de la app.
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {



    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;

        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

/*
Metodo para actualizar los datos que itroducimos
 */
    private void refrescarTodo() {
        listarProveedor();
        listarElectrodomestico();
        listarVenta();
        refrescar = false;
    }
/*
Contorlamos la accion y funcionalidad de los botones mediante el uso de un metodo switch
 */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch (comando) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "CrearTablaCocinas":
                try {
                    modelo.crearNuevaTabla();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "Salir":
                System.exit(0);
                break;
            case "Abrir Opciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {

                }
                break;
            case "GUARDAR":
                modelo.setPropValues(vista.optionDialog.txtIp.getText(),
                        vista.optionDialog.txtUsuario.getText(),
                        String.valueOf(vista.optionDialog.jpfContraseña.getPassword()),
                        String.valueOf(vista.optionDialog.jpfContraseñaAdministrador.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "AÑADIR PROVEEDOR":
                try {
                    modelo.insertarProveedor(
                            String.valueOf(vista.comboNombreP.getSelectedItem()),
                            String.valueOf(vista.comboNacionalidad.getSelectedItem()),
                            String.valueOf(vista.comboAniosMercado.getSelectedItem()));
                    listarProveedor();
                    JOptionPane.showMessageDialog(null, "Proveedor Creado con exito", "Info", JOptionPane.INFORMATION_MESSAGE);

                } catch (SQLException e1) {
                    e1.printStackTrace();
                    vista.jtProveedores.clearSelection();
                }

                break;
            case "MODIFICAR PROVEEDOR":
                try {
                    if (comprobarProveedorVacio()) {
                        JOptionPane.showMessageDialog(null, "Rellena los campos con obligatorios", "Info", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtProveedores.clearSelection();
                    } else {
                        modelo.modificarProveedor(
                                String.valueOf(vista.comboNacionalidad.getSelectedItem()),
                                String.valueOf(vista.comboNombreP.getSelectedItem()),
                                String.valueOf(vista.comboAniosMercado.getSelectedItem()),
                                Integer.parseInt((String.valueOf(vista.jtProveedores.getValueAt(vista.jtProveedores.getSelectedRow(), 0)))));
                        listarProveedor();
                        JOptionPane.showMessageDialog(null, "Proveedor Modificado correctamente", "Info", JOptionPane.INFORMATION_MESSAGE);
                    }

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "ELIMINAR PROVEEDOR":
                modelo.borrarProveedor(Integer.parseInt((String) (vista.jtProveedores.getValueAt(vista.jtProveedores.getSelectedRow(), 0))));
                JOptionPane.showMessageDialog(null, "Producto Eliminado con exito ", "Info", JOptionPane.INFORMATION_MESSAGE);
                listarProveedor();
                break;
            case "NUEVO":
                try {
                    if (comprobarElectromesticoVacios()) {
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Error", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtElectrodomestico.clearSelection();
                    } else {
                        modelo.insertarElectrodomestico(
                                String.valueOf(vista.comboElectrodomestico.getSelectedItem()),
                                vista.txtCantidad.getText(),
                                String.valueOf(vista.comboMarca.getSelectedItem()),
                                String.valueOf(vista.comboDimension.getSelectedItem()));
                        listarElectrodomestico();
                        JOptionPane.showMessageDialog(null, "Electrodomestico Añadido sin problema", "Info", JOptionPane.INFORMATION_MESSAGE);

                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }


                break;
            case "MODIFICAR ELEC":
                try {
                    if (comprobarElectromesticoVacios()) {
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Info", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtElectrodomestico.clearSelection();
                    } else {
                        modelo.modificarElectrodomestico(
                                String.valueOf(vista.comboElectrodomestico.getSelectedItem()),
                                vista.txtCantidad.getText(),
                                String.valueOf(vista.comboMarca.getSelectedItem()),
                                String.valueOf(vista.comboDimension.getSelectedItem()),
                                Integer.parseInt((String.valueOf(vista.jtElectrodomestico.getValueAt(vista.jtElectrodomestico.getSelectedRow(), 0)))));
                        listarElectrodomestico();
                        JOptionPane.showMessageDialog(null, "Electrodomestico Modificado con exito", "Info", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "MODIFICAR VENTA":
                try {
                    if (comprobarVentaVacia()) {
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Info", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtVenta.clearSelection();
                    } else {
                        modelo.modificarVenta(
                                vista.txtNombreCliente.getText(),
                                vista.txtDireccion.getText(),
                                String.valueOf(vista.comboVentaE.getSelectedItem()),
                                String.valueOf(vista.comboVentaP.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fechaFactura.getDate(),
                                Integer.parseInt((String.valueOf(vista.jtVenta.getValueAt(vista.jtVenta.getSelectedRow(), 0)))));
                        listarVenta();
                        JOptionPane.showMessageDialog(null, "Venta Modificada con exito", "Info", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "ELIMINAR ELEC":
                modelo.borrarElectrodomestico(Integer.parseInt((String) (vista.jtElectrodomestico.getValueAt(vista.jtElectrodomestico.getSelectedRow(), 0))));
                JOptionPane.showMessageDialog(null, "Electrodomestico eliminado con exito", "Info", JOptionPane.INFORMATION_MESSAGE);
                listarElectrodomestico();
                break;
            case "AÑADIR VENTA":
                try {
                    if (comprobarVentaVacia()) {
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Error", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtVenta.clearSelection();
                    } else {
                        modelo.insertarVenta(
                                vista.txtNombreCliente.getText(),
                                vista.txtDireccion.getText(),
                                String.valueOf(vista.comboVentaE.getSelectedItem()),
                                String.valueOf(vista.comboVentaP.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fechaFactura.getDate());
                        listarVenta();
                        JOptionPane.showMessageDialog(null, "Venta Creada", "Info", JOptionPane.INFORMATION_MESSAGE);

                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "ELIMINAR VENTA":
                modelo.borrarVenta(Integer.parseInt((String.valueOf(vista.jtVenta.getValueAt(vista.jtVenta.getSelectedRow(), 0)))));
                listarVenta();
                JOptionPane.showMessageDialog(null, "Venta eliminada cin exito", "Info", JOptionPane.INFORMATION_MESSAGE);
                break;
            case "BUSCAR":
                String buscarVenta=vista.txtBuscar.getText();
                try {
                    ResultSet resultSet=modelo.buscarVenta(buscarVenta);
                    cargarColumnasVenta(resultSet);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "VENTA POR NOMBRE":
                try {
                    modelo.clientePorNombre();

                    cargarColumnasVenta(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

        }
    }
/*
Metodo para listar los proveedores
 */
    private void listarProveedor () {
        try {
            vista.jtProveedores.setModel(cargarColumnasProveedor(modelo.consultarProveedor()));
            vista.comboVentaP.removeAllItems();
            for (int i = 0; i < vista.dtmProveedores.getRowCount(); i++) {
                vista.comboVentaP.addItem(
                        vista.dtmProveedores.getValueAt(i, 0) + " - " +
                                vista.dtmProveedores.getValueAt(i, 1) + " , " +
                                vista.dtmProveedores.getValueAt(i, 2) + " , " +
                                vista.dtmProveedores.getValueAt(i, 3));

            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    private DefaultTableModel cargarColumnasProveedor (ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> nombreColumna = new Vector<>();

        int contadorColumna = metaData.getColumnCount();

        for (int columna = 1; columna <= contadorColumna; columna++) {
            nombreColumna.add(metaData.getColumnName(columna));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, contadorColumna, data);

        vista.dtmProveedores.setDataVector(data, nombreColumna);
        return vista.dtmProveedores;
    }

    private void setDataVector (ResultSet rs,int contadorColumna, Vector<Vector<Object>>data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= contadorColumna; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void listarVenta () {
        try {
            vista.jtVenta.setModel(cargarColumnasVenta(modelo.consultarVenta()));
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    private DefaultTableModel cargarColumnasVenta (ResultSet resultSet)throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();

        Vector<String> nombreColumna = new Vector<>();

        int contadorColumna = metaData.getColumnCount();

        for (int columna = 1; columna <= contadorColumna; columna++) {
            nombreColumna.add(metaData.getColumnName(columna));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(resultSet, contadorColumna, data);

        vista.dtmVenta.setDataVector(data, nombreColumna);
        return vista.dtmVenta;
    }

    private void listarElectrodomestico () {
        try {
            vista.jtElectrodomestico.setModel(cargarColumnasElectrodomestico(modelo.consultarElectrodomestico()));
            vista.comboVentaE.removeAllItems();
            for (int i = 0; i < vista.dtmElectrodomesticos.getRowCount(); i++) {
                vista.comboVentaE.addItem(
                        vista.dtmElectrodomesticos.getValueAt(i, 0) + " - " +
                                vista.dtmElectrodomesticos.getValueAt(i, 1) + " , " +
                                vista.dtmElectrodomesticos.getValueAt(i, 2) + " , " +
                                vista.dtmElectrodomesticos.getValueAt(i, 3) + " , " +
                                vista.dtmElectrodomesticos.getValueAt(i, 4));
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

    }

    private DefaultTableModel cargarColumnasElectrodomestico (ResultSet resultSet)throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();

        Vector<String> nombreColumna = new Vector<>();

        int contadorColumna = metaData.getColumnCount();

        for (int columna = 1; columna <= contadorColumna; columna++) {
            nombreColumna.add(metaData.getColumnName(columna));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(resultSet, contadorColumna, data);

        vista.dtmElectrodomesticos.setDataVector(data, nombreColumna);
        return vista.dtmElectrodomesticos;
    }
/*
metodo para comprobar loscampos de venta estan vacios
 */
    private boolean comprobarVentaVacia () {
        return vista.txtNombreCliente.getText().isEmpty() ||
                vista.txtDireccion.getText().isEmpty() ||
                vista.comboVentaP.getSelectedIndex() == -1 ||
                vista.comboVentaE.getSelectedIndex() == -1 ||
                vista.txtPrecio.getText().isEmpty() ||
                vista.fechaFactura.getText().isEmpty();
    }

    /*
metodo para comprobar loscampos de electrodomesticos estan vacios
 */
    private boolean comprobarElectromesticoVacios () {
        return vista.comboElectrodomestico.getSelectedIndex() == -1 ||
                vista.txtCantidad.getText().isEmpty() ||
                vista.comboMarca.getSelectedIndex() == -1 ||
                vista.comboDimension.getSelectedIndex() == -1;

    }
    /*
    metodo para comprobar loscampos del proveedor estan vacios
     */
    private boolean comprobarProveedorVacio () {
        return vista.comboNombreP.getSelectedIndex() == -1 ||
                vista.comboNacionalidad.getSelectedIndex() == -1 ||
                vista.comboAniosMercado.getSelectedIndex() == -1;
    }
/*
Metodos para borrar los campos de VEENTA ELECTRODOMESTICO Y PROVEEDOR
 */
    private void borrarCamposVenta () {
        vista.txtNombreCliente.setText("");
        vista.txtDireccion.setText("");
        vista.comboVentaE.setSelectedIndex(-1);
        vista.comboVentaP.setSelectedIndex(-1);
        vista.txtPrecio.setText("");
        vista.fechaFactura.setText("");
    }

    private void borrarCamposElectrodomestico () {
        vista.comboElectrodomestico.setSelectedIndex(-1);
        vista.txtCantidad.setText("");
        vista.comboMarca.setSelectedIndex(-1);
        vista.comboDimension.setSelectedIndex(-1);
    }

    private void borrarCamposProveedor () {
        vista.comboNombreP.setSelectedIndex(-1);
        vista.comboNacionalidad.setSelectedIndex(-1);
        vista.comboAniosMercado.setSelectedIndex(-1);

    }
/*
MEtodo de las opciones de Usuario, Contnraseña
 */
    private void setOptions () {
        vista.optionDialog.txtIp.setText(modelo.getIP());
        vista.optionDialog.txtUsuario.setText(modelo.getUser());
        vista.optionDialog.jpfContraseña.setText(modelo.getPassword());
        vista.optionDialog.jpfContraseñaAdministrador.setText(modelo.getAdminPassword());
    }
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {
    }
/*
MEtodo para que todos los botones cumplan su funcionalidad
 */
    private void addActionListeners (Controlador listener){
        vista.btnAnadirP.addActionListener(listener);
        vista.btnModificarP.addActionListener(listener);
        vista.btnEliminarP.addActionListener(listener);

        vista.btnAnadirE.addActionListener(listener);
        vista.btnModificarE.addActionListener(listener);
        vista.btnEliminarE.addActionListener(listener);

        vista.btnAnadirVenta.addActionListener(listener);
        vista.btnModificarVenta.addActionListener(listener);
        vista.btnEliminarVenta.addActionListener(listener);
        vista.btnBuscar.addActionListener(listener);
        vista.btnVentaNombre.addActionListener(listener);

        vista.optionDialog.btnGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {


        if (e.getValueIsAdjusting()
                && !((ListSelectionModel)e.getSource()).isSelectionEmpty()){
            if (e.getSource().equals(vista.jtProveedores.getSelectionModel())){
                int fila=vista.jtProveedores.getSelectedRow();
                vista.comboNombreP.setSelectedItem(String.valueOf(vista.jtProveedores.getValueAt(fila,1)));
                vista.comboNacionalidad.setSelectedItem(String.valueOf(vista.jtProveedores.getValueAt(fila,2)));
                vista.comboAniosMercado.setSelectedItem(String.valueOf(vista.jtProveedores.getValueAt(fila,3)));
            }else if(e.getSource().equals(vista.jtElectrodomestico.getSelectionModel())){
                int fila=vista.jtElectrodomestico.getSelectedRow();
                vista.comboElectrodomestico.setSelectedItem(String.valueOf(vista.jtElectrodomestico.getValueAt(fila,1)));
                vista.txtCantidad.setText(String.valueOf(vista.jtElectrodomestico.getValueAt(fila,2)));
                vista.comboMarca.setSelectedItem(String.valueOf(vista.jtElectrodomestico.getValueAt(fila,3)));
                vista.comboDimension.setSelectedItem(String.valueOf(vista.jtElectrodomestico.getValueAt(fila,4)));
            }else if(e.getSource().equals(vista.jtVenta.getSelectionModel())){
                int fila=vista.jtVenta.getSelectedRow();
                vista.txtNombreCliente.setText(String.valueOf(vista.jtVenta.getValueAt(fila,1)));
                vista.txtDireccion.setText(String.valueOf(vista.jtVenta.getValueAt(fila,2)));
                vista.comboVentaP.setSelectedItem(String.valueOf(vista.jtVenta.getValueAt(fila,3)));
                vista.comboVentaE.setSelectedItem(String.valueOf(vista.jtVenta.getValueAt(fila,4)));
                vista.txtPrecio.setText(String.valueOf(vista.jtVenta.getValueAt(fila,5)));
                vista.fechaFactura.setDate(Date.valueOf(String.valueOf(vista.jtVenta.getValueAt(fila,6))).toLocalDate());
            }else if (e.getValueIsAdjusting()&&((ListSelectionModel)e.getSource()).isSelectionEmpty()&&!refrescar){
                if (e.getSource().equals(vista.jtProveedores.getSelectionModel())){
                    borrarCamposProveedor();
                }else if(e.getSource().equals(vista.jtElectrodomestico.getSelectionModel())){
                    borrarCamposElectrodomestico();
                }else if (e.getSource().equals(vista.jtVenta.getSelectionModel())){
                    borrarCamposVenta();
                }
            }
        }
    }
}

