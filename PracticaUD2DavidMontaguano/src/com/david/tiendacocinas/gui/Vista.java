package com.david.tiendacocinas.gui;

import com.david.tiendacocinas.base.enums.electrodomestico.Marca;
import com.david.tiendacocinas.base.enums.electrodomestico.NElectrodomestico;
import com.david.tiendacocinas.base.enums.proveedor.NProveedor;
import com.david.tiendacocinas.base.enums.proveedor.Nacionalidad;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{

    private final static String TITULO="TIENDA DE COCINAS DAVID MONTAGUANO";
    private JPanel panel1;
    private JTabbedPane tabbedPane;
    JMenuItem itemCrearTabla;

    /*
    Estructura de ELECTRODOMESTICO
     */
     JTextField txtCantidad;
     JComboBox comboElectrodomestico;
     JComboBox comboMarca;
     JComboBox comboDimension;
     JButton btnAnadirE;
     JButton btnModificarE;
     JButton btnEliminarE;
     JTable jtElectrodomestico;


    /*
    Estructura de PROVEEDORES
     */
     JComboBox comboNombreP;
     JComboBox comboNacionalidad;
     JComboBox comboAniosMercado;
     JButton btnAnadirP;
     JButton btnModificarP;
     JButton btnEliminarP;
     JTable jtProveedores;


    /*
    Estructura de VENTA
     */
     JTextField txtNombreCliente;
     JTextField txtDireccion;
     JComboBox comboVentaE;
     JComboBox comboVentaP;
     JTextField txtPrecio;
    JButton btnEliminarVenta;
     JButton btnModificarVenta;
     JButton btnAnadirVenta;
     DatePicker fechaFactura;
     JTable jtVenta;
     JButton btnBuscar;
     JTextField txtBuscar;
     JButton btnVentaNombre;

     /*
     TABLAS
      */

    DefaultTableModel dtmElectrodomesticos;
    DefaultTableModel dtmProveedores;
    DefaultTableModel dtmVenta;

    /*
    OPTION DIALOG
     */
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /*
    MENU
     */
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

     public Vista(){
         super(TITULO);
         initFrame();
     }

     private void initFrame(){
         this.setContentPane(panel1);
         this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
         this.pack();
         this.setVisible(true);
         this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
         this.setLocationRelativeTo(null);
         optionDialog = new OptionDialog(this);


         setMenu();
         setAdminDialog();
         setEnumComboBox();
         setTableModels();
     }

    private void setTableModels(){
        this.dtmElectrodomesticos=new DefaultTableModel();
        this.jtElectrodomestico.setModel(dtmElectrodomesticos);

        this.dtmProveedores=new DefaultTableModel();
        this.jtProveedores.setModel(dtmProveedores);

        this.dtmVenta= new DefaultTableModel();
        this.jtVenta.setModel(dtmVenta);

    }

    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemCrearTabla = new JMenuItem("Crear tabla Cocinas2");
        itemCrearTabla.setActionCommand("CrearTablaCocinas");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemCrearTabla);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);


    }

    private void setEnumComboBox(){
         /*
         PROVEEDORES
          */
         for(Nacionalidad constante:Nacionalidad.values()){
             comboNacionalidad.addItem(constante.getValor());
         }
         comboNacionalidad.setSelectedIndex(-1);

         for(NProveedor constante:NProveedor.values()){
             comboNombreP.addItem(constante.getValor());
         }
         comboNombreP.setSelectedIndex(-1);

         for(com.david.tiendacocinas.base.enums.proveedor.AniosMercado constante:com.david.tiendacocinas.base.enums.proveedor.AniosMercado.values()){
             comboAniosMercado.addItem(constante.getValor());
         }
         comboAniosMercado.setSelectedIndex(-1);



         /*
         ELECTRODOMESTICOS
          */

         for(NElectrodomestico constante: NElectrodomestico.values()){
             comboElectrodomestico.addItem(constante.getValor());
         }
         comboElectrodomestico.setSelectedIndex(-1);

         for(Marca constante: Marca.values()){
             comboMarca.addItem(constante.getValor());
         }
         comboMarca.setSelectedIndex(-1);

         for(com.david.tiendacocinas.base.enums.electrodomestico.Dimension constante: com.david.tiendacocinas.base.enums.electrodomestico.Dimension.values()){
             comboDimension.addItem(constante.getValor());
         }
         comboDimension.getSelectedIndex();
    }

    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("Abrir Opciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }
}
