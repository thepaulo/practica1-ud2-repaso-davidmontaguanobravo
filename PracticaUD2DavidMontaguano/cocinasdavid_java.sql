create database if not exists cocinasdavid;
--
use cocinasdavid;
--

create table if not exists venta(
                id_venta int primary key auto_increment ,
                nombre_cliente varchar(50),
                direccion varchar(50),
                id_proveedor int not null,
                id_electrodomestico int not null,
                precio float,
                fecha_factura date
);
--

create table if not exists proveedor(
                id_proveedor int primary key auto_increment,
                nombre_proveedor varchar(50),
                nacionalidad varchar(50),
                anios_mercado varchar(50)
);
--

create table if not exists electrodomestico(
                id_electrodomestico int primary key auto_increment,
                cantidad varchar (50),
		nombre_electrodomestico varchar (50),
                marca varchar(50),
                dimension varchar(100)


);
--
alter table venta
    add foreign key(id_proveedor)references proveedor(id_proveedor),
    add foreign key(id_electrodomestico)references electrodomestico(id_electrodomestico);